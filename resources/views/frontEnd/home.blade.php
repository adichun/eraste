<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ERASTE</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/styles.css') !!}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
</head>

<body>
    <div class="container-fluid box-container">
        <h4 class="title-brand">ERASTE</h4>
    </div>
    <!-- product -->
    <div class="container mt-5">
        <div class="row">
            @for ($i=0; $i<8; $i++) <div class="col-3 pb-5">
                <div class="card card-product">
                    <img class="card-img-top" src="assets/img/i.png" alt="Card image" style="width:100%">
                    <div class="card-body">
                        <h6 class="card-title title-product">Masker ala ala adichun dengan harga 1000..</h6>
                        <a href="#" class="btn btn-gold">Order Dong</a>
                    </div>
                </div>
        </div>
        @endfor
    </div>
    </div>
</body>

</html>