<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailproduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detailproduct', function (Blueprint $table) {
            $table->increments('id', true);
            $table->string('productname')->nullable();
            $table->integer('productid')->unsigned()->index()->foreignId('productid')->constrained('products');
            $table->string('slug')->nullable();
            $table->string('description')->nullable();
            $table->integer('price')->nullable();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detailproduct');
    }
}