<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailorders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detailorders', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->integer('idorder')->unsigned()->index()->foreignId('idorder')->constrained('orders');
            $table->integer('user_id')->unsigned()->index()->foreignId('user_id')->constrained('users');
            $table->integer('quantity')->nullable();
            $table->integer('price')->nullable();
            $table->string('customer_name')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detailorders');
    }
}